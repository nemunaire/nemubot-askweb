<?php include("header.html") ?>
    <section id="introduction">
      <article>
<?php

include("Question.class.php");
include("QuestionsFile.class.php");

@$id = $_GET['id'];

$fileQ = new QuestionsFile("questions.xml");
$question = $fileQ->get_question($id);
if (isset($question))
{
?>
        <h2><?php
if ($question->isValidated())
  echo "Cette question a déjà été validée ; si vous la modifiez, elle devra &ecirc;tre de nouveau valid&eacute;e";
else
  echo"Dernière chance pour changer d'avis";
?></h2>
        <p>
         <?php
           if ($question->get_writer() != null)
           {?><strong>Proposée par :</strong> <a href="mailto:<?php echo $question->get_writer()->getEmail(); ?>"><?php echo $question->get_writer()->getUsername(); ?></a><br><br><?php } ?>
          <strong>Cours concerné :</strong> <?php echo $question->getCourse()->getName(); ?><br><br>
          <strong>Question posée :</strong> <?php echo nl2br(htmlentities($question->getQuestion(), ENT_COMPAT, "UTF-8")); ?><br><br>
          <strong>Réponses valides exhaustives :</strong>
        </p>
<?php
           echo "<ul>";
           foreach($question->getAnswer() as $a)
             echo "<li>".htmlentities($a, ENT_COMPAT, "UTF-8")."</li>";
           echo "</ul>";
?>
        <p>
          Vous avez la possibilité de modifier votre question avant de la
          faire valider.<br>
          Faites vous plaisir.
        </p>
      </article>
      <article>
        <h2>Modifier la question ...</h2>
          <form method="post" action="questions.php">
            <input type="hidden" name="id" value=<?php echo $question->getId(); ?>>
            <input type="hidden" name="email" value="<?php if ($question->get_writer() != null) echo $question->get_writer()->getEmail(); else echo "bot@nemunai.re" ?>">
            <label for="course">De quelle matière s'agit-il ?</label><br>
            <select name="course" id="course">
<?php
include_once("Course.class.php");
$cs = Course::getCourses();
$qc = $question->getCourse();

foreach($cs as $c)
{
  if ($qc->getId() == $c->getId())
    print '<option selected="selected" value="'.$c->getId().'">'.$c->getName().'</option>';
  else
    print '<option value="'.$c->getId().'">'.$c->getName().'</option>';
}

?>
            </select>

            <p id="questionPart">
              <label id="q" for="question">Quelle est votre question ? </label><br>
              <textarea id="question" name="question" rows="3" cols="70"><?php echo htmlentities($question->getQuestion(), ENT_COMPAT, "UTF-8"); ?></textarea>
            </p>
            <p id="answerList">
              <label for="answer">Quelle est la réponse ?</label><br>
<?php
$max = 1;
foreach($question->getAnswer() as $k => $a)
{
  echo '<input type="text" name="answer'.$k.'" value="'.$a.'">';
  if ($max < $k)
    $max = $k;
}
print '<script type="text/javascript">var nbAnswer = '.($max+1).';</script>';
?>
            </p>

            <p>
              <input type="button" value="Ajouter une réponse supplémentaire"
                     onclick="add()"/>
            </p>
            <p>
              <input type="submit" name="send" value="Envoyer" />
            </p>
          </form>
        </article>
<?php
if (!$question->isValidated())
{
?>
        <article>
          <h2>... ou la confirmer telle quelle !</h2>
          <p>
            Si la question vous semble correcte, vous pouvez directement
            la confirmer ici :<br>
          </p>
        <form method="post"
              class="validation"
              action="confirmation.php">
          <input type="hidden" name="id" value="<?php echo $question->getId() ?>">
          <input type="submit" value="Je confirme">
        </form>
        <span style="clear: both; display: block;"></span>
<?php
}
}
else
  header("Location: ./");
?>
      </article>
    </section>
<?php include('footer.html') ?>
  </body>
</html>
