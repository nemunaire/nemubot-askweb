<?php

include("Question.class.php");
include("QuestionsFile.class.php");

include("header.html");

$id = $_GET['id'];
$file = new QuestionsFile("questions.xml");
$question = $file->get_question($id);

if (isset($question))
{
?>
    <section id="introduction">
      <article id="validate">
        <h2>Merci de votre participation !</h2>
        <p>
          Votre question a bien été enregistrée et est en attente de
          validation.<br>
          Un courrier électronique vient de vous &ecirc;tre envoyé pour que vous
          confirmiez que vous êtes bien l'auteur de la question.
        </p>
        <p>
          Vous pouvez proposer de nouvelles questions en cliquant
          <a href="./#newquestion">ici</a> !
        </p>
      </article>
    </section>
<?php
}
else
  header("Location: ./");
include('footer.html') ?>
  </body>
</html>
