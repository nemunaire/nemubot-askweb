<?php
require_once("QuestionsFile.class.php");

header("Content-type: text/plain");

@$filter = $_GET["filter"];

$file = new QuestionsFile("questions.xml");

foreach($file->get_questions() as $q)
{
  if (!empty($filter) && strtolower($q->getCourse()->getCode()) != $filter)
    continue;

  $a = $q->getAnswer();
  echo $q->getQuestion()."\n  > ".$a[0]."\n\n";
}
?>