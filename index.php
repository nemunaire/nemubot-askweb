<?php include("header.html") ?>
    <section id="introduction">
      <a id="hideShow" href="">
        <img src="topArrow.png" alt="icon">
      </a>
      <script type="text/javascript">
        $('#hideShow').click(function (event) {
            event.preventDefault();
            $('#intro_para').slideToggle("slow");
            return false;
          });
      </script>

      <article id="bonjour">
        <h2>Bonjour</h2>
        <p id="intro_para">
          Bienvenue sur le site de questions de Nemubot. Vous pouvez ici
          ajouter de nouvelles questions à Nemubot. Ces questions seront
          posées sur le chan irc.
        </p>
      </article>

      <a id="hideShowExp" href="">
        <img src="topArrow.png" alt="icon"/>
      </a>
      <script type="text/javascript">
        $('#hideShowExp').click(function (event) {
            event.preventDefault();
            $('#exp_para').slideToggle("slow");
            return false;
          });
      </script>

      <article id="explications">
        <h2>Nemubot AskWeb en 3 étapes</h2>
        <div id="exp_para">
          <h3>Nouvelle question</h3>
          <p>
            Commencez par entrer votre question ainsi que la ou les réponses
            associées. À noter que :
          </p>
          <ul>
            <li>
              Il est conseillé d'indiquer plusieurs fois vos réponses
              avec des variations telles que : "une chaussure"
              et "chaussure"
            </li>
            <li>
              Les questions possédant seulement 2 réponses possibles
              n'ont pas beaucoup d'intêret
            </li>
          </ul>

          <h3>Confirmer votre question</h3>
          <p>
            Une fois la question ajoutée, un email vous sera envoyé pour que
            vous confirmiez qu'il s'agisse bien de vous. Cliquez sur le lien
            pour confirmer.
          </p>

          <h3>Validation</h3>
          <p>
            Une fois confirmée, un email est envoyé à un autre membre du groupe
            pour qu'il valide la question. S'il juge votre question fausse
            ou éronée elle n'apparaitra jamais dans les qcm sur le chan.
          </p>
        </div>
      </article>
    </section>

    <section id="hello">
      <h2 id="newquestion">Nouvelle question</h2>
      <form id="formulaire" method="post" action="questions.php">
        <section id="Form">
          <aside id="info">
            <img src="left_triangle2.gif" id="arrow" alt="&lt;">
            <h3>Quelques détails</h3>
            <p>
              Ce champ est là à titre indicatif. Il va permettre à la personne
              qui valide votre question d'aller vérifier par lui même la
              véracité de votre réponse ; et il permet également de filtrer
              les questions en fonction des matières que chacun souhaite réviser.
            </p>
          </aside>

          <article id="list">
            <label for="course">De quelle matière s'agit-il ?</label><br>
            <select name="course" id="course">
<?php
include("Course.class.php");
$cs = Course::getCourses();

foreach($cs as $c)
  print '<option value="'.$c->getId().'">'.$c->getName().'</option>';
?>
            </select>
          </article>

          <aside id="quest">
            <img src="left_triangle2.gif" id="arrow2" alt="">
            <h3>La question</h3>
            <p>
              La question sera affichée sur une ligne. Donc pas la peine de
              faire de retour à la ligne.
            <p>
          </aside>

          <article>
            <p id="questionPart">
              <label id="q" for="question">
                Quelle est votre question ? </label><br>
              <textarea id="question" name="question"
                        rows="3" cols="70"
                        placeholder="Entrez votre question ici"></textarea>
            </p>
            <p id="answerList">
              <label for="answer">Quelle est la réponse ? </label><br/>
              <input id="answer" name="answer0" type="text">
            </p>

            <p>
              <script type="text/javascript">var nbAnswer = 1; document.write('<input type="button" value="Ajouter une réponse supplémentaire" onclick="add()">');</script>
              <noscript>Vous devez activer JavaScript pour ajouter des réponses</noscript>
            </p>
            <p>
              <label for="email">Merci d'indiquer votre adresse &eacute;lectronique :</label><br/>
              <input id="email" name="email" type="text" />
            </p>
            <p>
              <input type="submit" name="send" value="Envoyer" />
            </p>
          </article>
        </section>

      </form>
    </section>
<?php include('footer.html') ?>
  </body>
</html>
