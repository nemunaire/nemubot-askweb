<?php include("header.html") ?>
    <section id="introduction">
      <article id="validate">
        <h2><?php
include("Question.class.php");
include("QuestionsFile.class.php");

if (isset($_POST['id']))
{
  $id = $_POST['id'];
  $file = new QuestionsFile("questions.xml");
  $question = $file->get_question($id);

  if (!isset($question) || $question->isValidated() || !$question->isValidation())
    echo 'Votre question à déjà été validée, merci de ne pas vous acharner.';
  else
  {
    //Send confirmation to sender
    $subject = "[Nemubot] Refus d'une question";
    $headers = "From: Nemubot <bot@nemunai.re>";
    $message = "Bonjour,\n\n"
      ."L'une de vos questions proposée à Nemubot vient d'être refusée.\n\n"

      ."Rappel de la question :\n"
      ."  - Cours concerné : " . $question->getCourse()->getName() . "\n"
      ."  - Question posée : " . $question->getQuestion() . "\n"
      ."  - Les réponses valides sont :\n".$question->getAnswersMail()."\n"

      ."\nLa raison de ce refus est la suivante : \n"
      . $_POST['comment']

      ."\n\nVous pouvez modifier la question à cette adresse :\n"
      ."http://".$_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"])
      ."changeQuestion.php?id=".$question->getNormalId()

      . "\n\nMerci beaucoup de votre participation.\n"
      ."Cordialement,\n"

      ."-- \nNemubot\nQCM accessible sur le réseau IRC rezosup, cannal #epita-qcm";


    if ($question->mail_utf8($question->get_writer()->getEmail(), $subject, $message, $headers))
    {
      $file->save();
      echo 'Question refusée !';
    }
    else
      echo ("Une erreur s'est produite lors de l'envoi du courrier de confirmation. Veuillez contacter l'administrateur du service.");
  }
}
else
  header("Location: ./");
?></h2>
        <p>
          La question vient d'&ecirc;tre retourn&eacute;e &agrave; son auteur,
          il pourra la resoumettre d&egrave;s qu'il l'aura corrig&eacute;e.<br><br>

          <span style="text-decoration:line-through;">Amusez-vous</span> R&eacute;visez bien !
        </p>
      </article>
    </section>
  </body>
</html>
