<?php
include ("header.html");
?>
<section id="introduction">
  <article>
    <h2>
<?php
include("User.class.php");

$users = User::getUsers();

if (!array_key_exists($_GET['id'], $users))
  echo "Une erreur est survenue";
else if (!$users[$_GET['id']]->isValidated())
{
  $users[$_GET['id']]->set_validated(true);
  User::setUsers($users);
  echo "Vous êtes maintenant inscrit !";
}
else
  echo "Vous êtes déjà inscrit";
?></h2>
  </article>
</section>

</body>
</html>
