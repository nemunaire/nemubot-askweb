<?php

class Course
{
  private $id;
  private $name;
  private $code;
  private $branch;
  private $registerdate;
  private $validated = false;

  public function __construct($node = null)
  {
    if (isset($node))
    {
      $this->id = $node->getAttribute("xml:id");
      $this->registerdate = $node->getAttribute("registerdate");
      if (intval($node->getAttribute("validated")))
        $this->validated = true;
      $this->name = $node->getAttribute("name");
      $this->code = $node->getAttribute("code");
      $this->branch = $node->getAttribute("branch");
    }
  }

  public static function new_Course($name, $code, $branch = "")
  {
    $code = strtoupper($code);

    $c = new Course();
    $c->id = sha1($code);
    $c->registerdate = time();
    $c->name = $name;
    $c->code = $code;
    $c->branch = $branch;

    return $c;
  }

  public static function getCourses($filename = "courses.xml")
  {
    $courses = array();
    $treeXML = new DOMDocument('1.0', 'UTF-8');

    if (@$treeXML->load($filename))
    {
      $nodes = $treeXML->getElementsByTagName("course");
      foreach($nodes as $node)
      {
        $c = new Course($node);
        $courses[$c->id] = $c;
      }
    }

    return $courses;
  }

  public static function getCourse($id, $filename = "courses.xml")
  {
    $treeXML = new DOMDocument('1.0', 'UTF-8');
    
    if (@$treeXML->load($filename))
    {
      $c = $treeXML->getElementById($id);
      if ($c)
        return new Course($c);
    }
    return null;
  }

  public static function setCourses($courses, $filename = "courses.xml")
  {
    $treeXML = new DOMDocument('1.0', 'UTF-8');
    $root_node = $treeXML->createElement("users");
    $treeXML->appendChild($root_node);

    foreach ($courses as $course)
      $root_node->appendChild($course->to_xml($treeXML));

    $treeXML->formatOutput   = true;
    $treeXML->save($filename);
  }

  public function to_xml($root)
  {
    $qnode = $root->createElement("course");

    $qnode->setAttribute("xml:id", $this->id);
    $qnode->setAttribute("name", $this->name);
    $qnode->setAttribute("code", $this->code);
    $qnode->setAttribute("branch", $this->branch);
    $qnode->setAttribute("registerdate", $this->registerdate);
    $qnode->setAttribute("validated", intval($this->validated));

    return $qnode;
  }

  public function set_validated($validated)
  {
    $this->validated = $validated;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getCode()
  {
    return $this->code;
  }

  public function getBranch()
  {
    return $this->branch;
  }

  public function isValidated()
  {
    return $this->validated;
  }
}

/*
$cs = Course::getCourses();

foreach($cs as $c)
  $c->set_validated(true);
Course::setCourses($cs);
//*/
/*
$cs = Course::getCourses();

$c = Course::new_Course("Base de donnees", "RELA");
$cs[] = $c;

$c = Course::new_Course("Compression de donnee", "CODO");
$cs[] = $c;

$c = Course::new_Course("Protocole de liaisons", "PROLI");
$cs[] = $c;

$c = Course::new_Course("Typologie des langages", "TYLA", "TDA;SRC");
$cs[] = $c;

$c = Course::new_Course("Active directory", "ACDI");
$cs[] = $c;

$c = Course::new_Course("Graphes reseaux flots", "GRF");
$cs[] = $c;

Course::setCourses($cs);
//*/
?>