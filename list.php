<?php
include ("header.html");

session_start();

require_once("QuestionsFile.class.php");

@$filter = $_GET["filter"];

if (!empty($_SESSION["connected"]) && !empty($_POST["id"]))
{
  $file = new QuestionsFile("questions.xml");
  $q = $file->get_question($_POST["id"]);

  if (!empty($q))
  {
    $file->del_question($q);
    $file->save();
  }
  header("Location: ./list.php");
}
else
{
?>
    <section id="introduction" style="margin: auto -15%;">
      <article id="menu">.:
        <form method="get" action="?" style="float: right;">
        <?php if (isset($_GET["valid"])) echo '<input type="hidden" name="valid">'; ?>
        <label for="filter">Filtrer par code de cours :</label> <input type="text" id="filter" name="filter" value="<?php echo $filter; ?>"> <input type="submit" value="Filtrer">
        </form>
        <a href="?">Liste des questions non-validées</a> ::
        <a href="?<?php if (isset($_GET["valid"])) echo "valid&amp;"; ?>report">Liste des questions rapportées</a> ::
        <a href="?valid">Liste des questions validées</a> :.
      </article>
      <article id="allQuestions">
        <h2>Liste de toutes les questions <?php if (!isset($_GET["valid"])) echo "non-"; ?>validées<?php if (isset($_GET["report"])) echo " rapportées"; ?></h2>
        <table>
          <thead>
            <tr>
              <th>Cours</th>
              <th>Question</th>
              <th>Auteur</th>
              <th>Soumission</th>
              <th>Validateur</th>
            </tr>
          </thead>
          <tbody>
<?php

$file = new QuestionsFile("questions.xml");

date_default_timezone_set("Europe/Paris");

foreach($file->get_questions() as $q)
{
  if (!isset($_GET["valid"]))
  {
    if ($q->isValidated() && !$q->isReported())
      continue;
  }
  else
  {
    if (!$q->isValidated())
      continue;
  }

  if (isset($_GET["report"]))
  {
    if (!$q->isReported())
      continue;
  }

  if (!empty($filter) && $q->getCourse()->getCode() != $filter)
    continue;
  
  $id = $q->getId();
?>
<tr>
    <td><acronym title="<?php echo $q->getCourse()->getName(); ?>"><?php echo $q->getCourse()->getCode(); ?></acronym></td>
    <td><?php echo htmlentities($q->getQuestion(), ENT_COMPAT, "UTF-8"); ?></td>
    <td><?php if ($q->get_writer() != null) echo $q->get_writer()->getUsername(); ?></td>
    <td><?php echo strftime("%d/%m/%y %H:%M", $q->getAddedTime()); ?></td>
    <td><?php if ($q->get_validator() != null) echo $q->get_validator()->getUsername(); ?>
<?php
if (!empty($_SESSION["connected"]))
{
?>
     <div class="tooltip">
      <form method="post" action="confirmation.php?norandom"><input type="hidden" name="id" value="<?php echo $id ?>"><input type="submit" value="Relancer"></form>
      <form method="post" action="confirmation.php"><input type="hidden" name="id" value="<?php echo $id ?>"><input type="submit" value="Revalider"></form>
      <form method="get" action="changeQuestion.php"><input type="hidden" name="id" value="<?php echo $id ?>"><input type="submit" value="Modifier"></form>
      <form method="post" action="?del" onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer cette question ?');"><input type="hidden" name="id" value="<?php echo $id ?>"><input type="submit" value="Supprimer"></form>
      <form method="<?php if ($q->isReported()) echo "post"; else echo "get"; ?>" action="report.php"><input type="hidden" name="id" value="<?php echo $q->getReportId(); ?>"><input type="submit" value="<?php if ($q->isReported()) echo "Désignaler"; else echo "Signaler"; ?>"></form>
     </div>
<?php } ?>
    </td>
</tr>
<?php
}
?>
          </tbody>
        </table>
      </article>
<?php
}
include ("footer.html");
?>
  </body>
</html>
