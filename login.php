<?php
include ("header.html");

session_start();

require_once("User.class.php");

if (empty($_SESSION["connected"]))
{
  if (!empty($_POST["username"]) && !empty($_POST["password"]))
  {
    $us = User::getUsers();

    foreach($us as $u)
    {
      if (strtolower($u->getUsername()) == strtolower($_POST["username"]) && $u->canConnect($_POST["password"]))
      {
        $_SESSION["connected"] = true;
        $_SESSION["who"] = $u->getUsername();
        die("Vous êtes maintenant connectés !");
      }
    }
    die("Nom d'utilisateur ou mot de passe incorrect.");
  }
  else
  {
?>
    <section id="introduction">
      <article id="login">
        <h2>Se connecter</h2>
        <form method="post" action="login.php">
          <label for="username">Nom d'utilisateur :</label> <input type="text" name="username" id="username" maxlength="32"><br><br>

          <label for="password">Mot de passe :</label> <input type="password" name="password" id="password" maxlength="64"><br><br>

          <input type="submit" name="send" value="Se connecter">
        </form>
      </article>
    </section>
<?php
  }
}
else
{
  if (!empty($_POST["deco"]))
  {
    session_destroy();
    echo "<h2>Vous êtes maintenant déconnecté. À bientôt !</h2>";
  }
  else
  {
?>
    <section id="introduction">
      <article id="login">
        <h2>Bienvenue <?php echo $_SESSION["who"]; ?></h2>
        <form method="post" action="login.php">
          <input type="submit" name="deco" value="Se déconnecter">
        </form>
      </article>
      <article id="menu">.:
        <a href="list.php">Liste des questions non-valid&eacute;es</a> ::
        <a href="list.php?valid">Liste des questions valid&eacute;es</a> ::
        <a href="addCourse.php">Modifie la liste des cours</a> ::
        <a href="users.php">Modifie la liste des utilisateurs</a> :.
      </article>
    </section>
<?php
  }
}
include ("footer.html");
?>
  </body>
</html>
