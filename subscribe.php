<?php
include("header.html");

if (isset($_GET["new_admin"]))
{
  die("Fonction désactivée");
  if ($_POST["password"] == $_POST["passwordc"])
  {
    include("User.class.php");

    $users = User::getUsers();
    @$id = sha1($_POST["email"]);

    if (array_key_exists($id, $users))
    {
      $users[$id]->setPassword($_POST["password"]);
      User::setUsers($users);
      echo "Mot de passe défini avec succès.";
    }
    else
      echo("Utilisateur non trouvé");
  }
  else
    echo("Le mot de passe et sa confirmation sont différents.");
}
else
{
?>
    <section id="introduction">
      <article id="left">
        <h2>Inscription</h2>
        <p>
          Vous n'êtes pas encore inscrit sur le site mais rêvez de l'être ?
          Pas de panique. Il vous suffit simplement de mettre votre email
          dans le champs ci dessous.
        </p>
        <p>
          Vous allez alors recevoir un email de confirmation.<br/>
          Vous devez cliquer sur le lien d'activation pour confirmer
          votre demande.
        </p>

        <form id="formulaire_email" method="post" action="subscribeAction.php">
          <label for="email">Votre email : </label>
          <input id="email" name="email" type="text">

          <label for="login">Login : </label>
          <input id="login" name="login" type="text">

          <input type="submit" name="send" value="Envoyer">
        </form>
      </article>
      <article id="genpass">
        <h2>Administration</h2>
        <form id="formulaire_gen" method="post" action="?new_admin">
          <label for="email">Votre email : </label>
          <input id="email" name="email" type="text">

          <label for="password">Mot de passe : </label>
          <input id="passowrd" name="password" type="password">

          <label for="passwordc">Confirmation : </label>
          <input id="passowrdc" name="passwordc" type="password">

          <input type="submit" name="send" value="Envoyer">
        </form>
      </article>
    </section>
<?php } ?>
  </body>
</html>
