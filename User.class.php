<?php

class User
{
  private $id;
  private $username;
  private $password;
  private $email;
  private $registerdate;
  private $validated = false;
  private $admin = false;

  public function __construct($node = null)
  {
    if (isset($node))
    {
      $this->id = $node->getAttribute("xml:id");
      $this->registerdate = $node->getAttribute("registerdate");
      if (intval($node->getAttribute("validated")))
        $this->validated = true;
      if (intval($node->getAttribute("admin")))
        $this->admin = true;
      $this->username = $node->getAttribute("username");
      $this->password = $node->getAttribute("password");
      $this->email = $node->getAttribute("email");
    }
  }

  public static function new_User($email, $username = "", $password = null)
  {
    $u = new User();
    $u->id = sha1($email);
    $u->registerdate = time();
    $u->email = $email;
    $u->username = $username;
    if (isset($password))
      $u->password = $u->getPassword($username, $password);

    return $u;
  }

  public static function getUsers($filename = "users.xml")
  {
    $users = array();
    $treeXML = new DOMDocument('1.0', 'UTF-8');

    if (@$treeXML->load($filename))
    {
      $nodes = $treeXML->getElementsByTagName("user");
      foreach($nodes as $node)
      {
        $u = new User($node);
        $users[$u->id] = $u;
      }
    }

    return $users;
  }

  public static function getValidatedUsers($filename = "users.xml")
  {
    $users = array();
    $treeXML = new DOMDocument('1.0', 'UTF-8');

    if (@$treeXML->load($filename))
    {
      $nodes = $treeXML->getElementsByTagName("user");
      foreach($nodes as $node)
      {
        $u = new User($node);
        if ($u->isValidated())
          $users[] = $u;
      }
    }

    return $users;
  }

  public static function getUser($id, $filename = "users.xml")
  {
    $treeXML = new DOMDocument('1.0', 'UTF-8');
    
    if (@$treeXML->load($filename))
    {
      $u = $treeXML->getElementById($id);
      if (!empty($u))
        return new User($u);
    }
    return null;
  }

  public static function setUsers($users, $filename = "users.xml")
  {
    $treeXML = new DOMDocument('1.0', 'UTF-8');
    $root_node = $treeXML->createElement("users");
    $treeXML->appendChild($root_node);

    foreach ($users as $user)
      $root_node->appendChild($user->to_xml($treeXML));

    $treeXML->formatOutput   = true;
    $treeXML->save($filename);
  }

  public function to_xml($root)
  {
    $qnode = $root->createElement("user");

    $qnode->setAttribute("xml:id", $this->id);
    $qnode->setAttribute("username", $this->username);
    $qnode->setAttribute("password", $this->password);
    $qnode->setAttribute("email", $this->email);
    $qnode->setAttribute("registerdate", $this->registerdate);
    $qnode->setAttribute("validated", intval($this->validated));

    return $qnode;
  }

  public function canConnect($password)
  {
    $hash = $this->getPassword($this->username, $password);

    return ($hash == $this->password);
  }

  private function getPassword($username, $password)
  {
    return hash("whirlpool", $username.':'.$password);
  }

  public function setPassword($password)
  {
    $this->password = $this->getPassword($this->username, $password);
  }

  public function set_validated($validated)
  {
    $this->validated = $validated;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getUsername()
  {
    return $this->username;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function isValidated()
  {
    return $this->validated;
  }

  public function isAdmin()
  {
    return $this->admin;
  }
}

/*
$us = User::getUsers();

foreach($us as $u)
  $u->set_validated(true);
User::setUsers($us);
//*/
/*
$us = User::getUsers();

$u = User::new_User("ircquizz@p0m.fr", "nemunaire", "u6tn84");
$us[] = $u;

User::setUsers($us);
//*/
/*
$us = User::getUsers();

$u = User::new_User("bertrand@cournaud.fr", "Cccompany");
$us[] = $u;

$u = User::new_User("colona@ycc.fr", "colona");
$us[] = $u;

$u = User::new_User("ircquizz@23.tf", "maxence23");
$us[] = $u;

$u = User::new_User("ircquizz@p0m.fr", "nemunaire");
$us[] = $u;

$u = User::new_User("quentin.courtel@epita.fr", "Bob");
$us[] = $u;

$u = User::new_User("ghost_anarky@hotmail.com", "Anarky");
$us[] = $u;

User::setUsers($us);
//*/
/*
$us = User::getUsers();

$u = User::new_User("benjamin.frottier@gmail.com", "benf");
$u->set_validated(true);
$us[] = $u;

User::setUsers($us);
//*/
?>