<?php
include("header.html");

include("Question.class.php");
include("QuestionsFile.class.php");

$id = $_GET['id'];
$file = new QuestionsFile("questions.xml");
$question = $file->get_question($id);

?>
    <section id="introduction">
      <article>
        <h2>Validation de la question</h2>
        <h3>Rappel de la question</h3>
<?php
if (!isset($question))
  echo "La question n'existe pas.";
else
{
?>
        <p>
          <strong>Cours concerné :</strong> <?php echo $question->getCourse()->getName(); ?><br><br>
          <strong>Question posée :</strong> <?php echo htmlentities($question->getQuestion(), ENT_COMPAT, "UTF-8"); ?><br><br>
          <strong>Réponses valides exhaustives :</strong>
        </p>
<?php
           echo "<ul>";
           foreach($question->getAnswer() as $a)
             echo "<li>".htmlentities($a, ENT_COMPAT, "UTF-8")."</li>";
           echo "</ul>";
if ($question->isValidation())
{
?>
        <form method="post"
              class="invalidation"
              action="thanksRefused.php">
          <label for="comment">Précisez les raisons de votre refus</label>
          <input name="comment" type="textarea"
             rows="5" cols="50">

          <input type="hidden" name="id" value="<?php echo $question->getId() ?>">
          <input type="submit" value="Refuser la question">
        </form>
        <form method="post"
              class="validation"
              action="thanksValidation.php">
          <input type="hidden" name="id" value="<?php echo $question->getId() ?>">
          <input type="submit" value="Valider la question">
        </form>
        <span style="clear: both; display: block;"></span>
<?php } else echo "<strong>La question n'est pas ou plus dans une phase de validation.</strong>"; } ?>
      </article>
    </section>
<?php include('footer.html') ?>
  <body>
</html>
