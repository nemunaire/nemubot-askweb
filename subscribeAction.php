<?php

include("header.html");
include("User.class.php");

if (count($_POST))
{
  $email = $_POST['email'];
  $login = $_POST['login'];

  if (empty($email) || empty($login))
    die("Veuillez remplir tous les champs avant de valider le formulaire.");

  if (!preg_match("#^.+@[a-zA-Z0-9\._-]+\.[a-zA-Z]{2,5}$#", $email))
    die("Votre adresse électronique ne semble pas valide.");

  $us = User::getUsers();

  $user = User::new_User($email, $login);

  if (array_key_exists($user->getId(), $us))
    die("Un utilisateur avec la même adresse électronique existe déjà.");

  function mail_utf8($to, $subject = '(No subject)',
                     $message = '', $header = '')
  {
    $header_ = 'MIME-Version: 1.0' . "\r\n" .
      'Content-type: text/plain; charset=UTF-8' .   "\r\n";
    return (mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=',
                 $message, $header_ . $header));
  }

  $email = $_POST['email'];
  $subject = "[Nemubot] Confirmation d'inscription";
  $headers = "From: Nemubot <bot@nemunai.re>\n";
  $message = "Bonjour,\n"
    ."Vous avez demandé à être ajouté à la liste des participants "
    ."sur le site AskWeb.\n"
    ."Si c'est le cas, vous pouvez cliquer sur le lien suivant "
    ."pour confirmer :\n"

    . "http://".$_SERVER["SERVER_NAME"]
    . dirname($_SERVER["REQUEST_URI"]) . "/subscriptionConfirmation.php?id="
    . $user->getId()

    ."\n\n Si ce n'est pas le cas, merci de supprimer cet email\n"
    ."Cordialement,\n\n"
    ."-- \nNemubot";

  if (mail_utf8($email, $subject, $message, $headers))
  {
    $us[] = $user;
    User::setUsers($us);
    $message = true;
  }
  else
  {
    $message = false;
  }
}
else
  header("Location: ./");
?>

<section id="introduction">
<article>

<?php
  if ($message) {
?>

<h2>Merci</h2>
<p>
Un email a été envoyé à l'adresse mail que vous avez précisé.
Cliquez sur le lien contenu dans l'email pour confirmer votre inscription.
</p>

<?php
  } else {
?>
<h2>ERREUR</h2>
<p>Une erreur est survenue. Dommage.</p>
      <?php } ?>

</article>
</section>
</body>
</html>
