<?php include("header.html") ?>
    <section id="introduction">
      <article>
        <h2>Merci de votre participation</h2>
        <p>
          Votre question a bien été prise en compte.<br>
          Un courrier électronique a été envoyé à un membre aléatoire pour
          qu'il valide votre question.
        </p>
        <p>
          Nemubot vous remercie de l'aider à agrandir sa base de données.<br>
        </p>
        <p>
          Vous pouvez proposer de nouvelles questions en cliquant
          <a href="./#newquestion">ici</a> !
        </p>
      </article>
    </section>
  </body>
</html>

