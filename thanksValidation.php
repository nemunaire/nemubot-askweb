<?php include("header.html") ?>
    <section id="introduction">
      <article id="validate">
        <h2><?php
include("Question.class.php");
include("QuestionsFile.class.php");

if (isset($_POST['id']))
{
  $id = $_POST['id'];
  $file = new QuestionsFile("questions.xml");
  $question = $file->get_question($id);

  if (!isset($question) || $question->isValidated())
    echo 'Votre question à déjà été validée, merci de ne pas vous acharner.';
  else if (!$question->isValidation())
    echo 'Cette question n\'est pas dans une phase de validation. Impossible de la valider.';
  else
  {
    $question->validated();
    $file->save();
    echo 'Question validée !';
  }
}
else
  header("Location: ./");
?></h2>
        <p>
          Nemubot vous remercie de l'aider à agrandir sa base de données.<br><br>
          Vous pouvez vous aussi poser des questions à <a href="./" >cette adresse</a>,
          où bien simplement essayer de répondre aux questions déjà posées en tapant
          <code>!qcm</code> sur un cannal o&ugrave; nemubot est pr&eacute;sent.<br><br>

          <span style="text-decoration:line-through;">Amusez-vous</span> R&eacute;visez bien !
        </p>
      </article>
    </section>
  </body>
</html>
