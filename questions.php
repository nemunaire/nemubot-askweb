<?php

//error_reporting(E_ALL);
define("FILENAME", "questions.xml");

include("User.class.php");
include("Course.class.php");
include("Question.class.php");
include("QuestionsFile.class.php");

function isInUsersList($mail)
{
  $us = User::getUsers();

  foreach($us as $u)
  {
    if ($u->getEmail() == $mail/* && $u->isValidated()*/)
      return $u;
  }

  return false;
}

function isInCoursesList($course)
{
  return Course::getCourse($course);
}

// Change this variable depending on the server
$confirmationAddress = "confirmation.php?id=";

if (isset($_POST['send']))
{
  //Gets parameters: course, question and answers
  $question = $_POST["question"];
  $answers = array();

  foreach ($_POST as $key => $value)
  {
    if (preg_match("#^answer[0-9]+$#", $key))
      if (!empty($value) && !in_array($value, $answers))
        $answers[] = $value;
  }

  //Check we have at least a question and an answer
  if (empty($question))
    die("Veuillez indiquer une question !");
  else if (count($answers) <= 0)
    die("Veuillez indiquer au moins une réponse correcte !");
  else if (empty($_POST["email"]) || ($usr = isInUsersList($_POST["email"])) == false)
    die("Veuillez indiquer une adresse &eacute;lectronique valide !");
  else if (empty($_POST["course"]) || ($course = isInCoursesList($_POST["course"])) == null)
    die("Veuillez indiquer un cours valide !");
  else
  {
    if (!empty($_POST["id"]))
    {
      $file = new QuestionsFile("questions.xml");
      $quest = $file->get_question($_POST["id"]);

      $quest->setQuestion($question);
      $quest->setAnswer($answers);
      $quest->setCourse($course);
    }
    else
    {
      $quest = Question::new_Question($question, $answers, $course->getId());

      $file = new QuestionsFile('questions.xml');
      $file->add_question($quest);
    }
    $quest->set_writer($usr);
    $quest->set_validator($usr);

    $file->save();

    //Send confirmation to sender
    $subject = "[Nemubot] Confirmation d'une question";
    $headers = "From: Nemubot <bot@nemunai.re>";
    $message = "Bonjour,\n\n"
      ."Une nouvelle question a été proposée à Nemubot en utilisant cette \n"
      ."adresse email.\n\n"

      ."Rappel de la question :\n"
      ."  - Cours concerné : " . $course->getName() . "\n"
      ."  - Question posée : " . $question . "\n"
      ."  - Les réponses valides sont :\n".$quest->getAnswersMail()."\n"

      ."Si vous avez effectivement posé cette question, merci de cliquer sur \n"
      ."le lien ci-dessous pour confirmer.\n"
      ."Si vous ne comprenez rien à cet email ou que vous n'avez pas posté de \n"
      ."nouvelles questions, vous pouvez supprimer ce message.\n\n"

      ."Adresse de confirmation de la question :\n"
      ."http://".$_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"])
      ."changeQuestion.php?id=".$quest->getId()

      . "\n\nMerci beaucoup de votre participation.\n"
      ."Cordialement,\n"

      ."-- \nNemubot\nQCM accessible sur le réseau IRC rezosup, cannal #epita-qcm";


    if (!empty($_POST["id"]))
      header("Location: ./changeQuestion.php?id=".$quest->getId());
    else if ($quest->mail_utf8($usr->getEmail(), $subject, $message, $headers))
      header("Location: ./thanks.php?id=" . $quest->getId());
    else
      echo ("Une erreur s'est produite lors de l'envoi du courrier de confirmation. Veuillez contacter l'administrateur du service.");
  }
}
else
  header("Location: ./");
?>
