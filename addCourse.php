<?php
include ("header.html");

require_once("Course.class.php");

session_start();

if (empty($_SESSION["connected"]))
  die("<h2>Vous devez être connecté pour accédé à cette partie</h2>");

if (isset($_POST["send"]))
{
  if (empty($_POST["fullname"]))
    echo "<h2>Le nom complet n'est pas optionnel !</h2>";
  else if (empty($_POST["code"]))
    echo "<h2>Le code du cours n'est pas optionnel !</h2>";
  else
  {
    $cs = Course::getCourses();

    $c = Course::new_Course($_POST["fullname"], $_POST["code"], $_POST["branch"]);
    if (array_key_exists($c->getId(), $cs))
      echo "<h2>Ce cours est déjà enregistré !</h2>";
    else
    {
      $cs[] = $c;

      Course::setCourses($cs);

      echo "<h2>Cours ajouté avec succès !</h2>";
    }
  }
}
else if (isset($_GET["del"]))
{
  $cs = Course::getCourses();

  if (array_key_exists($_GET["del"], $cs))
  {
    unset($cs[$_GET["del"]]);
    Course::setCourses($cs);

    echo "<h2>Cours supprimé avec succès !</h2>";
  }
  else
    echo "<h2>Le cours n'a pas été trouvé</h2><meta http-equiv=\"refresh\" content=\"2;URL=./addCourse.php\">";
}
else
{
?>
    <section id="introduction">
      <article id="ajoutCours">
        <h2>Ajouter un cours</h2>
          <form method="post" action="addCourse.php">
            <label for="fullname">Nom complet :</label> <input type="text" name="fullname" id="fullname" maxlength="64"><br><br>

            <label for="code">Code :</label> <input type="text" name="code" id="code" maxlength="10"><br>
            Par exemple : CODO pour Compression de données<br><br>

            <label for="branch">Branche d'électifs :</label> <input type="text" name="branch" id="branch" maxlength="100"><br>
            Précisez s'il s'agit d'un cours réservé aux MTM, TDA ou SRC (par exemple). Si plusieurs sont concernés, séparez par des ;. Si tous son concernés, laissez vide.<br><br>
            <input type="submit" name="send" value="Ajouter">
          </form>
      </article>

      <article id="delCours">
        <h2>Supprimer un cours</h2>
<?php
$cs = Course::getCourses();
foreach($cs as $c)
  echo $c->getName()." (".$c->getCode()." dans ".$c->getBranch().") <a href=\"addCourse.php?del=".$c->getId()."\">Supprimer</a><br>";
?>
      </article>
    </section>
<?php
}
include ("footer.html");
?>
  </body>
</html>
