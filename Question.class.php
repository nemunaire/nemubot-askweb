<?php

include_once("User.class.php");
include_once("Course.class.php");

class Question
{
  private $id;
  private $question;
  private $course;
  private $answers = array();
  private $added_time;
  private $validated = false;
  private $reported = false;
  private $validator = "";
  private $writer = "";

  public function __construct($node = null)
  {
    if (isset($node))
    {
      $this->id = $node->getAttribute("xml:id");
      $this->added_time = $node->getAttribute("addedtime");
      if (intval($node->getAttribute("validated")))
        $this->validated = true;
      if (intval($node->getAttribute("reported")))
        $this->reported = true;
      $this->validator = $node->getAttribute("validator");
      $this->writer = $node->getAttribute("writer");
      $this->question = $node->getAttribute("question");
      $this->course = $node->getAttribute("course");

      $answers = $node->getElementsByTagName("answer");
      for ($i = 0; $i < $answers->length; $i++)
        $this->answers[] = $answers->item($i)->getAttribute("answer");
    }
  }

  public static function new_Question($question, $answers, $course = null)
  {
    $q = new Question();
    $q->id = md5(time().$question);
    $q->added_time = time();
    $q->question = $question;
    $q->course = $course;

    if (!empty($answers))
    {
      if (is_array($answers))
      {
        foreach ($answers as $ans)
          $q->answers[] = $ans;
      }
      else
        $q->answers[] = $answers;
    }

    return $q;
  }

  public function set_validator($val)
  {
    $this->validator = $val->getId();
  }

  public function get_writer()
  {
    $u = User::getUser($this->writer);
    if (isset($u))
      return $u;
    else
      return User::getUser("nemubot");
  }

  public function get_validator()
  {
    return User::getUser($this->validator);
  }

  public function print_test()
  {
    echo 'Cours concerné : ' . $this->course . "<br>";
    echo 'La question est : ' . $this->question . "<br>";
    echo 'Les réponses sont : ' . print_r($this->answers, true) . "<br/>";
  }

  /**
   * Generate and fill an answer node
   * @param $answer The answer string
   * @param $scrore The score given by this answer
   */
  private function gen_anode($treeXML, $answer, $score = 1)
  {
    $anode = $treeXML->createElement("answer");

    $anode->setAttribute("answer", $answer);
    $anode->setAttribute("score", $score);

    return $anode;
  }

  /**
   * Generate XML corresponding to this question
   */
  public function to_xml($root)
  {
    $qnode = $root->createElement("question");

    $qnode->setAttribute("xml:id", $this->id);
    $qnode->setAttribute("addedtime", $this->added_time);
    $qnode->setAttribute("reported", intval($this->reported));
    $qnode->setAttribute("validated", intval($this->validated));
    $qnode->setAttribute("validator", $this->validator);
    $qnode->setAttribute("writer", $this->writer);
    $qnode->setAttribute("course", $this->course);
    $qnode->setAttribute("question", $this->question);

    foreach ($this->answers as $ans)
      $qnode->appendChild($this->gen_anode($root, $ans));

    return $qnode;
  }

  public function validated()
  {
    $this->validated = true;
    $this->reported = 0;

    //Return to normal ID
    $this->getNormalId();
  }

  public function getAddedTime()
  {
    return $this->added_time;
  }

  public function getReportId()
  {
    return md5($this->id);
  }

  public function setReported($reported = true)
  {
    $this->reported = $reported;
  }

  public function isReported()
  {
    return $this->reported;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getValidatorId()
  {
    $this->id = md5($this->added_time.$this->validator);
    return $this->id;
  }

  public function getNormalId()
  {
    $this->id = md5($this->added_time.$this->writer);
    return $this->id;
  }

  public function isValidated()
  {
    return $this->validated;
  }

  public function isValidation()
  {
    return $this->id == md5($this->added_time.$this->validator) && $this->validator != $this->writer;
  }

  public function getCourse()
  {
    return Course::getCourse($this->course);
  }

  public function setCourse($course)
  {
    $this->course = $course->getId();
  }

  public function getQuestion()
  {
    return $this->question;
  }

  public function setQuestion($question)
  {
    if ($this->question != $question)
    {
      $this->question = $question;
      $this->validated = false;
    }
  }

  public function getAnswer()
  {
    return $this->answers;
  }

  public function getAnswersMail()
  {
    $str = "";
    foreach($this->answers as $a)
      $str .= "    * ".$a."\n";
    return $str;
  }

  public function set_writer($writer)
  {
    $this->writer = $writer->getId();
  }

  public function convert()
  {
    foreach (Course::getCourses() as $c)
    {
      if ($c->getCode() == $this->course)
      {
        $this->course = $c->getId();
        return $c->getId();
      }
    }
    return $this->course;
    /*foreach (User::getUsers() as $u)
    {
      if ($u->getEmail() == $this->validator)
      {
        $this->validator = $u->getId();
        return $u->getId();
      }
    }
    return $this->validator;*/
  }

  public function setAnswer($answers)
  {
    if ($this->answers != $answers)
    {
      $this->answers = array();
      $this->validated = false;

      if (!empty($answers))
      {
        if (is_array($answers))
        {
          foreach ($answers as $ans)
            $this->answers[] = $ans;
        }
        else
          $this->answers[] = $answers;
      }
    }
  }

  function mail_utf8($to, $subject = '(No subject)',
                     $message = '', $header = '')
  {
    $header_ = 'MIME-Version: 1.0' . "\r\n" .
      'Content-type: text/plain; charset=UTF-8' .   "\r\n";
    return (mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=',
                 $message, $header_ . $header));
  }

}

?>