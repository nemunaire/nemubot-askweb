<?php include("header.html") ?>
    <section id="introduction">
      <article id="report">
        <h2><?php
include("Question.class.php");
include("QuestionsFile.class.php");

session_start();

if (isset($_GET['id']) || (isset($_POST['id']) && !empty($_SESSION["connected"])))
{
  if (isset($_POST['id']) && !empty($_SESSION["connected"]))
    $id = $_POST['id'];
  else
    $id = $_GET['id'];

  $file = new QuestionsFile("questions.xml");
  foreach($file->get_questions() as $q)
  {
    if ($q->getReportId() == $id)
    {
      $qm = $file->get_question($q->getId());
      $qm->setReported(!(isset($_POST['id']) && !empty($_SESSION["connected"])));
      $id = null;
      $file->save();
      break;
    }
  }
  if (isset($id))
    echo 'La question n\'a pas été trouvée';
  else if (isset($_POST['id']) && !empty($_SESSION["connected"]))
    echo 'Le signalement de la question a bien été retiré';
  else
    echo 'La question a été rapporté avec succès';
}
else
{
  echo "Aucun identifiant n'a été passé à la page";
  header("Location: ./");
}
?></h2>
        <p>
          Nemubot vous remercie de l'aider à améliorer la qualité de sa base de données.<br><br>
          Vous pouvez vous aussi poser des questions à <a href="./" >cette adresse</a>,
          où bien simplement essayer de répondre aux questions déjà posées en tapant
          <code>!qcm</code> sur un cannal o&ugrave; nemubot est pr&eacute;sent.<br><br>

          <span style="text-decoration:line-through;">Amusez-vous</span> R&eacute;visez bien !
        </p>
      </article>
    </section>
  </body>
</html>
