<?php

include("Question.class.php");
include("QuestionsFile.class.php");

session_start();

@$id = $_POST['id'];

$fileQ = new QuestionsFile("questions.xml");
$question = $fileQ->get_question($id);

if (!empty($question))
{
  if ($question->get_writer() != null)
    $src_mail = $question->get_writer()->getEmail();
  else
    $src_mail = "";

  //Build email list
  $users = User::getValidatedUsers();
  $nbUsers = count($users);

  if (isset($_GET["norandom"]) && !empty($_SESSION["connected"]))
  {
    for ($random = 0; $random < $nbUsers; $random++)
    {
      if ($question->get_validator()->getEmail() == $users[$random]->getEmail())
        break;
    }
  }
  else
  {
    //Pick a random email
    do
    {
      $random = rand(1, $nbUsers)-1;
    }
    while ($src_mail == $users[$random]->getEmail() || $question->get_validator()->getEmail() == $users[$random]->getEmail());
  }


  if (!isset($users[$random]))
    die("Impossible de trouver d'utilisateur compatible, veuillez en informer l'administrateur");

  $question->set_validator($users[$random]);

  $subject = "[Nemubot] Validation d'une question";
  $headers = "From: Nemubot <bot@nemunai.re>\n";
  $message = "Bonjour,\n\n"
    ."Une nouvelle question a été proposée à Nemubot.\n\n"
    ."Vous avez été sélectionné pour valider la question.\n\n"

    ."Voici la question :\n"
    .'  - Cours concerné : ' . $question->getCourse()->getName() . "\n"
    .'  - Question posée : ' . $question->getQuestion() . "\n"
    ."  - Les réponses valides sont :\n" . $question->getAnswersMail() . "\n"


    ."Adresse de confirmation de la question :\n"
    ."http://".$_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"])
    ."validation.php?id=".$question->getValidatorId()

    ."\n\nMerci beaucoup de votre participation\n"
    ."Cordialement,\n"

    ."-- \nNemubot\nQCM accessible sur le réseau IRC rezosup, cannal #epita-qcm";


  if ($question->mail_utf8($users[$random]->getEmail(), $subject, $message, $headers))
  {
    $fileQ->save();
    header("Location: ./thanksConfirmation.php");
  }
  else
    die("Une erreur s'est produite lors de l'envoie du mail");
}
else
  die("ID de question invalide ou déjà validé.");
?>
